# Templates for CEUR-WS

## Criteria

- All design options (1 column, 2 columns, etc.) should be built on the basis of one class.
- The base class should be fairly modern and supported. Preferably if it is included in TeX-distributions:
  - LNCS -- 2018/03/10, not in TeXlive;
  - acmart -- 2019/10/19, in TeXlive;
  - revtex -- 2019/01/18, in TeXlive.
- Metadata markup should be descriptive, not presentative:
  - LNCS -- mostly presentative + descriptive metadata markup;
  - acmart -- descriptive metadata markup;
  - revtex -- descriptive metadata markup.
- The base class should be as flexible as possible.

## Directories

### ilaria

- 1 column ceur-template from LNCS.
- 2 column template following ACM-sigchi.

### dmitry

- acmart based template.
- LNCS-like authors formatting in 2-column is fragile (not supported by Word, LibreOffice etc.). 
- Maybe we need to try standard author placement (like in revtex).
 
