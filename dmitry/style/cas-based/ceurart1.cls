% Copyright 2019, CEUR Workshop Proceedings (CEUR-WS.org)
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either
% version 1.3 of this license or (at your option) any
% later version.
% The latest version of the license is in
%    http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of
% LaTeX version 2005/12/01 or later.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ceurart}
[2019/12/01 v0.01 Typesetting articles for CEUR Workshop Proceedings (CEUR-WS.org)]

\RequirePackage{expl3,xparse}

\newif\if@twocolumn
\DeclareOption{twocolumn}{\@twocolumntrue}

\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{acmart}%
}
\PassOptionsToClass{sigconf,10pt}{acmart}
\ProcessOptions\relax
\LoadClass{acmart}


%
% Load Common items
%

\RequirePackage{cas-common}

\define@choicekey*{ACM@}{acmcopyrightmode}[%
  \acm@copyrightinput\acm@copyrightmode]{none,%
    ccby
  }{%
  \@printpermissiontrue
  \@printcopyrighttrue
  \@acmownedtrue
  \ifnum\acm@copyrightmode=0\relax % none
   \@printpermissionfalse
   \@printcopyrightfalse
   \@acmownedfalse
  \fi
  \ifnum\acm@copyrightmode=1\relax % ccby
    \@printpermissionfalse
    \@acmownedfalse
  \fi}
\def\setcopyright#1{\setkeys{ACM@}{acmcopyrightmode=#1}}
\setcopyright{ccby}
\def\@copyrightowner{%
  \ifcase\acm@copyrightmode\relax % none
  \or % ccby
  Copyright for this paper by its authors. Use permitted under Creative Commons License Attribution 4.0 International (CC BY 4.0)\@.
  \fi}

%% FIX
\@ACM@manuscriptfalse
\let\@acmISBN\@empty
\let\@acmPrice\@empty
\let\@acmDOI\@empty

\def\@mkbibcitation{}

\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}

\let\Conference\acmConference

\def\@secfont{\bfseries\sffamily\Large\section@raggedright}
\def\@subsecfont{\bfseries\sffamily\Large\section@raggedright}

\@ACM@journalfalse

\def\@printtopmatter{%
  \ifx\@startPage\@empty
     \gdef\@startPage{1}%
  \else
     \setcounter{page}{\@startPage}%
  \fi
  \thispagestyle{firstpagestyle}%
  \noindent
  \if@twocolumn\twocolumn[\box\mktitle@bx]\else\box\mktitle@bx\par\fi
}

%% Copyright

% \def\copyrightline{%
%   \ifx\@hideLIPIcs\@undefined
%     \ifx\@EventLogo\@empty
%     \else
%       \setbox\@tempboxa\hbox{\includegraphics[height=42\p@]{\@EventLogo}}%
%       \rlap{\hspace\textwidth\hspace{-\wd\@tempboxa}\hspace{\z@}%
%             \vtop to\z@{\vskip-0mm\unhbox\@tempboxa\vss}}%
%     \fi
%     \scriptsize
%     \vtop{\hsize\textwidth
%       \nobreakspace\par
%       \@Copyright
%       \ifx\@EventLongTitle\@empty\else\@EventLongTitle.\\\fi
%       \ifx\@EventEditors\@empty\else
%         \@Eds: \@EventEditors
%         ; Article~No.\,\@ArticleNo; pp.\,\@ArticleNo:\thepage--\@ArticleNo:\pageref{LastPage}%
%         \\
%       \fi
%       \setbox\@tempboxa\hbox{\includegraphics[height=14\p@,trim=0 15 0 0]{lipics-logo-bw}}%
%       \hspace*{\wd\@tempboxa}\enskip
%       \href{https://www.dagstuhl.de/lipics/}%
%            {Leibniz International Proceedings in Informatics}\\
%       \smash{\unhbox\@tempboxa}\enskip
%       \href{https://www.dagstuhl.de}%
%            {Schloss Dagstuhl -- Leibniz-Zentrum f{\"u}r Informatik, Dagstuhl Publishing, Germany}}%
%   \fi}

\DeclareRobustCommand\copyrightclause[1]{%
  \def\@copyrightclause{#1}%
}
\let\@copyrightclause\@empty
% \copyrightclause{\textcolor{red}{Author: Please provide a copyright holder}}



%% CC-BY logo width
\newdimen\ccLogoWidth

\def\@copyrightLine{%
  \ifcase\acm@copyrightmode\relax % none
  \or % ccby
  \setbox\@tempboxa\hbox{\includegraphics[height=14\p@,clip]{cc-by}}%
  \ccLogoWidth=\dimexpr\wd\@tempboxa
  \@rightskip\@flushglue \rightskip\@rightskip
  % \hangindent\dimexpr\wd\@tempboxa+0.5em\relax
  \begin{minipage}{\ccLogoWidth}
    \href{https://creativecommons.org/licenses/by/4.0/}%
    {\lower\baselineskip\hbox{\unhcopy\@tempboxa}}\enskip
  \end{minipage}
    \textcopyright\ \@copyrightyear\ %
    \ifx\@copyrightclause\@empty%
      \textcolor{red}{Author: Please fill in the \string\copyrightclause\space macro}%
    \else\@copyrightclause%
    \fi
\fi}

\def\@ceurLogoLine{%
  % set width of ceur-ws logo equals to width of cc-by logo
  \setbox\@tempboxa\hbox{\includegraphics[width=\ccLogoWidth,clip]{ceur-ws-logo}}
  \@rightskip\@flushglue \rightskip\@rightskip
  % \hangindent\dimexpr\wd\@tempboxa+0.5em\relax
  \begin{minipage}{\ccLogoWidth}
    \href{http://ceur-ws.org/}%
    {\lower\baselineskip\hbox{\unhcopy\@tempboxa}}\enskip
  \end{minipage}
  {CEUR Workshop Proceedings (\href{http://ceur-ws.org/}{CEUR-WS.org})}%
}

\def\maketitle{%
  \@ACM@maketitle@typesettrue
  \if@ACM@anonymous
    % Anonymize omission of \author-s
    \ifnum\num@authorgroups=0\author{}\fi
  \fi
  \begingroup
  \let\@vspace\@vspace@orig
  \let\@vspacer\@vspacer@orig
  \let\@footnotemark\@footnotemark@nolink
  \let\@footnotetext\@footnotetext@nolink
  \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
  \hsize=\textwidth
  \def\@makefnmark{\hbox{\@textsuperscript{\@thefnmark}}}%
  \@mktitle\if@ACM@sigchiamode\else\@mkauthors\fi\@mkteasers
  \@printtopmatter
  \if@ACM@sigchiamode\@mkauthors\fi
  \setcounter{footnote}{0}%
  \def\@makefnmark{\hbox{\@textsuperscript{\normalfont\@thefnmark}}}%
  \@titlenotes
  \@subtitlenotes
  \@authornotes
  \let\@makefnmark\relax
  \let\@thefnmark\relax
  \let\@makefntext\noindent
  \ifx\@empty\thankses\else
    \footnotetextauthorsaddresses{%
      \def\par{\let\par\@par}\parindent\z@\@setthanks}%
  \fi
  \ifx\@empty\@authorsaddresses\else
     \if@ACM@anonymous\else
       \if@ACM@journal@bibstrip
         \footnotetextauthorsaddresses{%
           \def\par{\let\par\@par}\parindent\z@\@setauthorsaddresses}%
       \fi
     \fi
  \fi
  \if@ACM@nonacm\else\footnotetextcopyrightpermission{%
    \if@ACM@authordraft
        \raisebox{-2ex}[\z@][\z@]{\makebox[0pt][l]{\large\bfseries
            Unpublished working draft. Not for distribution.}}%
       \color[gray]{0.9}%
    \fi
    \parindent\z@\parskip0.1\baselineskip
    \if@ACM@authorversion\else
      \if@printpermission\@copyrightpermission\par\fi
    \fi
    \if@ACM@manuscript\else
       \if@ACM@journal@bibstrip\else % Print the conference information
         {\itshape \acmConference@shortname, \acmConference@date, \acmConference@venue}\par
       \fi
    \fi
    \if@printcopyright
      % \copyright\ \@copyrightyear\ \@copyrightowner\\
      \@copyrightLine\\
    \else
      \@copyrightyear.\
    \fi
    \@ceurLogoLine
    \if@ACM@manuscript
      Manuscript submitted to ACM\\
    \else
      \if@ACM@authorversion
          This is the author's version of the work. It is posted here for
          your personal use. Not for redistribution. The definitive Version
          of Record was published in
          \if@ACM@journal@bibstrip
            \emph{\@journalName}%
          \else
            \emph{\@acmBooktitle}%
          \fi
          \ifx\@acmDOI\@empty
          .
          \else
            , \@formatdoi{\@acmDOI}.
          \fi\\
        \else
          \if@ACM@nonacm\else
            \if@ACM@journal@bibstrip
              \@permissionCodeOne/\@acmYear/\@acmMonth-ART\@acmArticle
              \ifx\@acmPrice\@empty\else\ \$\@acmPrice\fi\\
              \@formatdoi{\@acmDOI}%
            \else % Conference
              \ifx\@acmISBN\@empty\else ACM~ISBN~\@acmISBN
              \ifx\@acmPrice\@empty.\else\dots\$\@acmPrice\fi\\\fi
              \ifx\@acmDOI\@empty\else\@formatdoi{\@acmDOI}\fi%
            \fi
          \fi
        \fi
      \fi}
    \fi
  \endgroup
  \setcounter{footnote}{0}%
  \@mkabstract
  \if@ACM@printccs
  \ifx\@concepts\@empty\else\bgroup
      {\@specialsection{CCS Concepts}%
         \@concepts\par}\egroup
     \fi
   \fi
   \ifx\@keywords\@empty\else\bgroup
      {\if@ACM@journal
         \@specialsection{Additional Key Words and Phrases}%
       \else
         \@specialsection{Keywords}%
       \fi
         \@keywords}\par\egroup
   \fi
  \andify\authors
  \andify\shortauthors
  \global\let\authors=\authors
  \global\let\shortauthors=\shortauthors
  \if@ACM@printacmref
     \@mkbibcitation
  \fi
  \hypersetup{%
    pdfauthor={\authors},
    pdftitle={\@title},
    pdfsubject={\@concepts},
    pdfkeywords={\@keywords},
    pdfcreator={LaTeX with ceurart
      \csname ver@ceurart.cls\endcsname\space
      and hyperref
      \csname ver@hyperref.sty\endcsname}}%
  \global\@topnum\z@ % this prevents floats from falling
                     % at the top of page 1
  \global\@botnum\z@ % we do not want them to be on the bottom either
  \@printendtopmatter
  \@afterindentfalse
  \@afterheading
}

%% Authors

\def\@mkauthors{\begingroup
  % \hsize=\textwidth
  % \ifcase\ACM@format@nr
  % \relax % manuscript
  % \@mkauthors@i
  % \or % acmsmall
  %   \@mkauthors@i
  % \or % acmlarge
  %   \@mkauthors@i
  % \or % acmtog
  %   \@mkauthors@i
  % \or % sigconf
  %   \@mkauthors@iii
  % \or % siggraph
  %   \@mkauthors@iii
  % \or % sigplan
  %   \@mkauthors@iii
  % \or % sigchi
  %   \@mkauthors@iii
  % \or % sigchi-a
  %   \@mkauthors@iv
  % \fi
  \@mkauthors@ceur
  \endgroup
}


\ExplSyntaxOn
\makeatletter


\ExplSyntaxOff

\def\@authorfont{\normalsize\normalfont}
\def\@affiliationfont{\normalsize\normalfont}

% \def\@typeset@author@line{%
%   \andify\@currentauthors\par\noindent
%   \@currentauthors\def\@currentauthors{}%
%   \ifx\@currentaffiliations\@empty\else
%     \andify\@currentaffiliations
%       \unskip, {\@currentaffiliations}\par
%   \fi
%   \def\@currentaffiliations{}
% }

\def\@typeset@author@line{%
  \andify\@currentauthors
  \@currentauthors\def\@currentauthors{}%
  % \ifx\@currentaffiliations\@empty\else
  %   \andify\@currentaffiliations
  %     \unskip, {\@currentaffiliations}\par
  % \fi
  % \def\@currentaffiliations{}
}


\def\@mkauthors@ceur{%
  \def\@currentauthors{}%
  \def\@currentaffiliations{}%
  \global\let\and\@typeset@author@line
  \def\@author##1{%
    \ifx\@currentauthors\@empty
      \gdef\@currentauthors{\@authorfont##1}%
    \else
       \g@addto@macro{\@currentauthors}{\and##1}%
    \fi
    \gdef\and{}}%
  \def\email##1##2{}%
  \def\affiliation##1##2{%
    \def\@tempa{##2}\ifx\@tempa\@empty\else
       \ifx\@currentaffiliations\@empty
          \gdef\@currentaffiliations{%
            \setkeys{@ACM@affiliation@}{obeypunctuation=false}%
            \setkeys{@ACM@affiliation@}{##1}%
            \@affiliationfont##2}%
       \else
         \g@addto@macro{\@currentaffiliations}{\and
           \setkeys{@ACM@affiliation@}{obeypunctuation=false}%
           \setkeys{@ACM@affiliation@}{##1}##2}%
      \fi
    \fi
     \global\let\and\@typeset@author@line}%
  \global\setbox\mktitle@bx=\vbox{\noindent\box\mktitle@bx\par\medskip
    \noindent\addresses\@typeset@author@line
   \par\medskip}%
}


% for review
\AtBeginDocument{%
  \fancypagestyle{standardpagestyle}{%
    \fancyhf{}%
    \renewcommand{\headrulewidth}{\z@}%
    \renewcommand{\footrulewidth}{\z@}%
    \def\@acmArticlePage{%
      \ifx\@acmArticle\empty%
        \if@ACM@printfolios\thepage\fi%
      \else%
        \@acmArticle\if@ACM@printfolios:\thepage\fi%
      \fi%
    }%
    \if@ACM@journal@bibstrip
      \ifcase\ACM@format@nr
      \relax % manuscript
      \fancyhead[LE]{\ACM@linecountL\if@ACM@printfolios\thepage\fi}%
      \fancyhead[RO]{\if@ACM@printfolios\thepage\fi}%
      \fancyhead[RE]{\@shortauthors}%
      \fancyhead[LO]{\ACM@linecountL\shorttitle}%
      \if@ACM@nonacm\else%
        \fancyfoot[RO,LE]{\footnotesize Manuscript submitted to ACM}
      \fi%
      \or % acmsmall
      \fancyhead[LE]{\ACM@linecountL\@headfootfont\@acmArticlePage}%
      \fancyhead[RO]{\@headfootfont\@acmArticlePage}%
      \fancyhead[RE]{\@headfootfont\@shortauthors}%
      \fancyhead[LO]{\ACM@linecountL\@headfootfont\shorttitle}%
      \if@ACM@nonacm\else%
        \fancyfoot[RO,LE]{\footnotesize \@journalNameShort, Vol. \@acmVolume, No.
          \@acmNumber, Article \@acmArticle.  Publication date: \@acmPubDate.}%
      \fi
      \or % acmlarge
      \fancyhead[LE]{\ACM@linecountL\@headfootfont
        \@acmArticlePage\quad\textbullet\quad\@shortauthors}%
      \fancyhead[LO]{\ACM@linecountL}%
      \fancyhead[RO]{\@headfootfont
        \shorttitle\quad\textbullet\quad\@acmArticlePage}%
      \if@ACM@nonacm\else%
        \fancyfoot[RO,LE]{\footnotesize \@journalNameShort, Vol. \@acmVolume, No.
          \@acmNumber, Article \@acmArticle.  Publication date: \@acmPubDate.}%
      \fi
      \or % acmtog
      \fancyhead[LE]{\ACM@linecountL\@headfootfont
        \@acmArticlePage\quad\textbullet\quad\@shortauthors}%
      \fancyhead[LO]{\ACM@linecountL}%
      \fancyhead[RE]{\ACM@linecountR}%
      \fancyhead[RO]{\@headfootfont
        \shorttitle\quad\textbullet\quad\@acmArticlePage\ACM@linecountR}%
      \if@ACM@nonacm\else%
        \fancyfoot[RO,LE]{\footnotesize \@journalNameShort, Vol. \@acmVolume, No.
          \@acmNumber, Article \@acmArticle.  Publication date: \@acmPubDate.}%
      \fi
    \else % Proceedings
      \fancyfoot[C]{\if@ACM@printfolios\footnotesize\thepage\fi}%
      \fancyhead[LO]{\ACM@linecountL\@headfootfont\shorttitle}%
      \fancyhead[RE]{\@headfootfont\@shortauthors\ACM@linecountR}%
      \if@ACM@nonacm\else%
        \fancyhead[LE]{\ACM@linecountL\@headfootfont\footnotesize
          \acmConference@shortname,
          \acmConference@date, \acmConference@venue}%
        \fancyhead[RO]{\@headfootfont
          \acmConference@shortname,
          \acmConference@date, \acmConference@venue\ACM@linecountR}%
      \fi
    \fi
  \else % Proceedings
    \fancyfoot[C]{\if@ACM@printfolios\footnotesize\thepage\fi}%
    \fancyhead[LO]{\ACM@linecountL\@headfootfont\shorttitle}%
    \fancyhead[RE]{\@headfootfont\@shortauthors\ACM@linecountR}%
    \if@ACM@nonacm\else%
      \fancyhead[LE]{\ACM@linecountL\@headfootfont
        \acmConference@shortname,
        \acmConference@date, \acmConference@venue}%
      \fancyhead[RO]{\@headfootfont
        \acmConference@shortname,
        \acmConference@date, \acmConference@venue\ACM@linecountR}%
    \fi
  \fi
  \if@ACM@sigchiamode
    \fancyheadoffset[L]{\dimexpr(\marginparsep+\marginparwidth)}%
  \fi
  \if@ACM@timestamp
    \fancyfoot[LO,RE]{\ACM@timestamp}
  \fi
}%
\pagestyle{standardpagestyle}
}
